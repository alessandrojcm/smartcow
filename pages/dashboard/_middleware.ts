import type { NextFetchEvent, NextRequest } from "next/server";
import { NextResponse } from "next/server";

const PUBLIC_FILE = /\.(.*)$/;

export function middleware(request: NextRequest, _: NextFetchEvent) {
  const pathname = request.nextUrl.pathname;
  // If it is a file then redirect as normal
  // see https://github.com/vercel/examples/blob/main/edge-functions/i18n/pages/_middleware.ts
  if (PUBLIC_FILE.test(pathname)) {
    return NextResponse.next();
  }
  if (request.url.search("/api") !== -1) {
    return NextResponse.next();
  }
  if (!request.cookies?.session) {
    return NextResponse.redirect(new URL("/login", request.url));
  }
  return NextResponse.next();
}
