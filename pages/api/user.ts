import { NextApiRequest, NextApiResponse } from "next";

import { CookieJar } from "@core/cookies";
import { User } from "@core/types/User";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const cookies = new CookieJar(req, res);
  const session = cookies.get<User>("session");
  if (!session) {
    res.status(401);
  }
  res.json(session);
  res.end();
}
