import { NextApiRequest, NextApiResponse } from "next";
import { CookieJar } from "@core/cookies";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const cookies = new CookieJar(req, res);
  if (
    req.body?.email === process.env.AUTHORIZED_EMAIL &&
    req.body?.password === process.env.AUTHORIZED_PASSWORD
  ) {
    cookies.set("session", {
      id: 1,
      email: "13bala90@gmail.com",
      firstname: "Balamurali",
      lastname: "A",
    });
    res.status(200);
  } else {
    res.status(401);
  }
  res.end();
}
