import type { AppProps } from "next/app";

import "@common/styles/global.scss";
import "normalize.css/normalize.css";
import Sidebar from "@layout/default/Sidebar";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Sidebar />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
