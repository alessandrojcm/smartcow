import { ElementType } from "react";
import classNames from "classnames";

import styles from "./IconButton.module.scss";

interface IconButtonProps {
  icon: ElementType;
  className?: string;
}

function IconButton(props: IconButtonProps) {
  return (
    <button className={classNames(styles.iconButton, props.className)}>
      <props.icon />
    </button>
  );
}

IconButton.displayName = "IconButton";

export default IconButton;
