import { Root, SeparatorProps } from "@radix-ui/react-separator";
import classNames from "classnames";

import styles from "./Separator.module.scss";

export const Separator = (props: SeparatorProps) => (
  <Root
    {...props}
    className={classNames(styles.separator, props.className ?? "")}
  />
);
