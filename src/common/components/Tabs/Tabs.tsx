import * as Tabs from "@radix-ui/react-tabs";
import {
  TabsContentProps,
  TabsListProps,
  TabsTriggerProps,
} from "@radix-ui/react-tabs";
import classNames from "classnames";

import styles from "./Tabs.module.scss";

export const TabsRoot = (props: Tabs.TabsProps) => (
  <Tabs.Root {...props} className={classNames(props.className)} />
);
export const TabList = (props: TabsListProps) => (
  <Tabs.List
    {...props}
    className={classNames(props.className, styles.tabsList)}
  />
);
export const TabTrigger = (props: TabsTriggerProps) => (
  <Tabs.Trigger
    {...props}
    className={classNames(props.className, styles.tabsTrigger)}
  />
);

export const TabContent = (props: TabsContentProps) => (
  <Tabs.Content {...props} />
);
