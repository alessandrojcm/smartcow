import * as Toggle from "@radix-ui/react-toggle";
import { ToggleProps } from "@radix-ui/react-toggle";
import classNames from "classnames";

import styles from "./Tag.module.scss";

interface TagProps extends ToggleProps {}

function Tag({ children, ...rest }: TagProps) {
  return (
    <Toggle.Root {...rest} className={classNames(styles.tag, rest.className)}>
      {children}
    </Toggle.Root>
  );
}

Tag.displayName = "Tag";

export default Tag;
