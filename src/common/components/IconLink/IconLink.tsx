import { PropsWithChildren } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import classNames from "classnames";

import styles from "./IconLink.module.scss";

interface IconLinkProps {
  href: string;
  label: string;
}

function IconLink({ children, href, label }: PropsWithChildren<IconLinkProps>) {
  const { asPath } = useRouter();
  return (
    <Link href={href} passHref>
      <span
        className={classNames(styles.iconButton, {
          [styles.iconButtonselected]: asPath === href,
        })}
      >
        {children}
        <span style={{ display: "none" }}>{label}</span>
      </span>
    </Link>
  );
}

IconLink.displayName = "IconLink";

export default IconLink;
