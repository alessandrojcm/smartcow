import { ChangeEvent, ReactNode } from "react";
import * as Label from "@radix-ui/react-label";
import classNames from "classnames";

import styles from "./Input.module.scss";

interface InputProps
  extends Partial<
    Pick<HTMLInputElement, "type" | "placeholder" | "required" | "value" | "id">
  > {
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  label?: ReactNode;
  name: string;
  className?: string;
}

function Input({ name, label, onChange, className, ...rest }: InputProps) {
  if (!label) {
    return <input {...rest} onChange={(e) => onChange && onChange(e)} />;
  }
  return (
    <div className={classNames(styles.input, className)}>
      <Label.Root htmlFor={name} className={styles.labelStyle}>
        {label}
      </Label.Root>
      <input {...rest} onChange={(e) => onChange && onChange(e)} />
    </div>
  );
}

Input.displayName = "Input";

export default Input;
