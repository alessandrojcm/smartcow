import Cookies from "cookies";
import { NextApiRequest, NextApiResponse } from "next";

const cookiesOptions = {
  httpOnly: true,
  secure: process.env.NODE_ENV === "production",
};

export class CookieJar {
  private cookie: Cookies;

  constructor(req: NextApiRequest, res: NextApiResponse) {
    this.cookie = new Cookies(req, res, {
      ...cookiesOptions,
      ...(cookiesOptions.secure && {
        keys: [process.env.COOKIE_SIGNING_KEY as string],
      }),
    });
  }

  set(name: string, data: Record<any, any>) {
    this.cookie.set(name, JSON.stringify(data), {
      httpOnly: true,
      secure: cookiesOptions.secure,
    });
  }

  get<T>(name: string): T | null {
    if (!this.cookie.get(name)) {
      return null;
    }
    return JSON.parse(this.cookie.get(name)!) as T;
  }
}
