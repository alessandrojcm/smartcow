import ky from "ky";

import { User } from "@core/types/User";

export async function login(email: string, password: string): Promise<boolean> {
  return ky
    .post("/api/login", {
      json: {
        email,
        password,
      },
    })
    .then((r) => r.status === 200);
}

export async function getUser(): Promise<User> {
  return ky.get("/api/user").json<User>();
}
