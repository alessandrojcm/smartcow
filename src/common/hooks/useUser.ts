import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import { getUser } from "@core/api/loginApi";
import { type User } from "@core/types/User";

export function useUser() {
  const [user, setUser] = useState<User | null>(null);
  const { asPath } = useRouter();
  useEffect(() => {
    if (asPath.search("dashboard") === -1) {
      return;
    }
    getUser()
      .then(setUser)
      .catch(() => setUser(null));
  }, [setUser, asPath]);
  return user;
}
