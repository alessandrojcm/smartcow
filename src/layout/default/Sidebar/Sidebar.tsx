import * as Avatar from "@radix-ui/react-avatar";
import Link from "next/link";

import styles from "./Sidebar.module.scss";
import { useUser } from "@common/hooks/useUser";
import Logo from "@public/icons/logo.svg";
import Camera from "@public/icons/camera.svg";
import VideoCollection from "@public/icons/video-collection.svg";
import IconLink from "@common/components/IconLink";
import Separator from "@common/components/Separator";

function Sidebar() {
  const user = useUser();
  return (
    <aside className={styles.sidebar}>
      <section className={styles.menu}>
        <Logo />
        {user ? (
          <>
            <span style={{ marginBottom: "auto" }}>
              <IconLink href={"/dashboard"} label={"Home"}>
                <Camera />
              </IconLink>
              <IconLink href={"/dashboard/saved-videos"} label={"My videos"}>
                <VideoCollection />
              </IconLink>
            </span>
            <Link href={"/dashboard/my-account"} passHref>
              <Avatar.Root style={{ cursor: "pointer" }}>
                <Avatar.Image src={"/images/Avatar.png"} />
              </Avatar.Root>
            </Link>
          </>
        ) : null}
      </section>
      <Separator orientation={"vertical"} />
    </aside>
  );
}

Sidebar.displayName = "Sidebar";

export default Sidebar;
