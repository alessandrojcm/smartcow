import { SyntheticEvent, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Head from "next/head";

import styles from "./Login.module.scss";
import { login } from "@core/api/loginApi";
import Input from "@common/components/Input";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const { push } = useRouter();

  const onSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    login(email, password)
      .then(() => {
        push("/dashboard");
      })
      .catch(() => setError(true));
  };

  return (
    <>
      <Head>
        <title>Log in</title>
      </Head>
      <main className={styles.login}>
        <h1>Sign in</h1>
        <form onSubmit={onSubmit}>
          <div className={styles.form} style={{ marginLeft: "-30%" }}>
            <Input
              label={"Email address"}
              name={"email"}
              placeholder={"Enter your email"}
              type={"email"}
              required
              onChange={(e) => setEmail(e.target.value)}
            />
            <Input
              label={
                <span className={styles.passwordLabel}>
                  <p>Password</p>
                  <p>Forgot?</p>
                </span>
              }
              name={"password"}
              type={"password"}
              placeholder={"Enter your password"}
              required
              onChange={(e) => setPassword(e.target.value)}
            />
            {error ? (
              <p className={styles.error}>Wrong username or password</p>
            ) : null}
            <button>Login</button>
            <p>
              New here? <Link href={"/signup"}>Sign up</Link>
            </p>
          </div>
        </form>
      </main>
    </>
  );
}

Login.displayName = "Login";

export default Login;
