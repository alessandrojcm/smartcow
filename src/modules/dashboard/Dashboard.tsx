import { useState } from "react";
import Head from "next/head";
import dynamic from "next/dynamic";

import MainVideoScreen from "./components/MainVideoScreen";
import styles from "./Dashboard.module.scss";
import ImageSelector from "./components/ImageSelector";
import { ImageKeys } from "./types";
import VideoDescription from "./components/VideoDescription";
import {
  TabContent,
  TabList,
  TabsRoot,
  TabTrigger,
} from "@common/components/Tabs";

type Alignment = "left" | "center" | "right";

const actorsOptions = Object.values(ImageKeys).map((k) => ({
  name: k,
  alt: k,
}));

const VoiceSelector = dynamic(() => import("./components/VoiceSelector"));
const AlignmentSelector = dynamic(
  () => import("./components/AlignmentSelector")
);
const BackgroundSelector = dynamic(
  () => import("./components/BackgroundSelector")
);

function Dashboard() {
  const [selectedImage, setSelectedImage] = useState<ImageKeys | string>(
    ImageKeys.YoYo
  );
  const [Alignment, setAlignment] = useState<Alignment>("center");
  const [title, setTitle] = useState("Actor");
  return (
    <>
      <Head>
        <title>Dashboard - {title}</title>
      </Head>
      <main className={styles.dashboard}>
        <VideoDescription />
        <div className={styles.mainView}>
          <MainVideoScreen
            className={styles.videoScreen}
            selectedImage={selectedImage}
            alt={selectedImage}
          />
          <TabsRoot
            defaultValue={"Actor"}
            className={styles.tabs}
            onValueChange={setTitle}
          >
            <TabList
              aria-label={"Edit your video"}
              style={{ marginBottom: "1rem" }}
            >
              <TabTrigger value={"Actor"}>Actor</TabTrigger>
              <TabTrigger value={"Voice"}>Voice</TabTrigger>
              <TabTrigger value={"Alignment"}>Alignment</TabTrigger>
              <TabTrigger value={"Background"}>Background</TabTrigger>
            </TabList>
            <TabContent value={"Actor"}>
              <ImageSelector
                onImageSelect={setSelectedImage}
                images={actorsOptions}
                selectedImage={selectedImage as ImageKeys}
              />
            </TabContent>
            <TabContent value={"Voice"}>
              <VoiceSelector />
            </TabContent>
            <TabContent value={"Alignment"}>
              <AlignmentSelector
                setAlignment={setAlignment}
                selectedAlignment={Alignment}
              />
            </TabContent>
            <TabContent value={"Background"}>
              <BackgroundSelector />
            </TabContent>
          </TabsRoot>
        </div>
      </main>
    </>
  );
}

Dashboard.displayName = "Dashboard";

export default Dashboard;
