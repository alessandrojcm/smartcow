export function getImageSrc(key: string) {
  return `/images/${key}.png`;
}
