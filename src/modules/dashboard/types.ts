export enum ImageKeys {
  Anna = "Anna",
  May = "May",
  Myke = "Myke",
  Peter = "Peter",
  Skye = "Skye",
  Vincent = "Vincent",
  YoYo = "YoYo",
}

export type Alignment = "left" | "center" | "right";
