import Image from "next/image";
import classNames from "classnames";

import styles from "./ImageSelector.module.scss";
import { getImageSrc } from "../../utils";
import { ImageKeys } from "../../types";

export type ImageSrc = { name: string; alt: string };

export interface ImageSelectorProps {
  images: Array<ImageSrc>;
  onImageSelect?: (actor: string) => void;
  selectedImage?: ImageKeys;
}

function ImageSelector(props: ImageSelectorProps) {
  return (
    <section className={styles.actorSelector}>
      {props.images.map((a) => (
        <figure
          key={a.alt}
          className={classNames(styles.actorElement, {
            [styles.selectedActor]: props.selectedImage === a.name,
          })}
          onClick={() => props?.onImageSelect && props.onImageSelect(a.alt)}
        >
          <Image
            layout={"intrinsic"}
            width={200}
            height={125}
            src={getImageSrc(a.name)}
            alt={a.alt}
          />
          <figcaption>{a.alt}</figcaption>
        </figure>
      ))}
    </section>
  );
}

ImageSelector.displayName = "ActorSelector";

export default ImageSelector;
