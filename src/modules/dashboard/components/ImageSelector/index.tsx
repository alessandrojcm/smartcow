import ImageSelector, { ImageSelectorProps, ImageSrc } from "./ImageSelector";

export type { ImageSelectorProps, ImageSrc };

export default ImageSelector;
