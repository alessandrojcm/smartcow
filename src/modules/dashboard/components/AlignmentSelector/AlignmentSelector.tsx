import classNames from "classnames";

import { type Alignment } from "../../types";
import styles from "./AlignmentSelector.module.scss";

interface AlignmentSelectorProps {
  setAlignment: (a: Alignment) => void;
  selectedAlignment: Alignment;
}

function AlignmentSelector({
  setAlignment,
  selectedAlignment,
}: AlignmentSelectorProps) {
  return (
    <section className={styles.alignmentContainer}>
      <button
        onClick={() => setAlignment("left")}
        className={classNames(styles.alignmentButton, {
          [styles.alignmentButtonActive]: selectedAlignment === "left",
        })}
      >
        Left
      </button>
      <button
        onClick={() => setAlignment("center")}
        className={classNames(styles.alignmentButton, {
          [styles.alignmentButtonActive]: selectedAlignment === "center",
        })}
      >
        Center
      </button>
      <button
        onClick={() => setAlignment("right")}
        className={classNames(styles.alignmentButton, {
          [styles.alignmentButtonActive]: selectedAlignment === "right",
        })}
      >
        Right
      </button>
    </section>
  );
}

AlignmentSelector.displayName = "AlignmentSelector";

export default AlignmentSelector;
