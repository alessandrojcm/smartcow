import classNames from "classnames";

import SoundWave from "@public/icons/sound-wave.svg";
import SoundWavePlaying from "@public/icons/sound-wave-playing.svg";
import Play from "@public/icons/play.svg";
import Pause from "@public/icons/pause.svg";
import styles from "./VoiceSelector.module.scss";

function WavePlayer(props: { label: string; playing?: boolean }) {
  return (
    <div
      className={classNames(styles.audioPlayer, {
        [styles.playing]: props.playing,
        [styles.paused]: !props.playing,
      })}
    >
      {props.playing ? (
        <Pause className={styles.controls} />
      ) : (
        <Play className={styles.controls} />
      )}
      <span className={styles.audioPlayerSoundWave}>
        <p>{props.label}</p>
        {props.playing ? (
          <SoundWavePlaying className={styles.soundWave} />
        ) : (
          <SoundWave className={styles.soundWave} />
        )}
      </span>
    </div>
  );
}

function VoiceSelector() {
  return (
    <section className={styles.voiceSelectorContainer}>
      <WavePlayer label={"Asian"} playing />
      <WavePlayer label={"British"} />
      <WavePlayer label={"American"} />
    </section>
  );
}

VoiceSelector.displayName = "VoiceSelector";

export default VoiceSelector;
