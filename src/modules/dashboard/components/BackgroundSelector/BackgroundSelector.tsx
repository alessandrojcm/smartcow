import * as Accordion from "@radix-ui/react-accordion";

import ImageSelector, { type ImageSrc } from "../ImageSelector";
import styles from "./BackgroundSelector.module.scss";
import Arrow from "@public/icons/arrow-up.svg";
import Separator from "@common/components/Separator";

const IMAGES: Array<ImageSrc> = [
  {
    alt: "Upload",
    name: "upload",
  },
  {
    alt: "Books",
    name: "books",
  },
  {
    alt: "Desk",
    name: "desk",
  },
  {
    alt: "Meeting Room",
    name: "meeting-room",
  },
  {
    alt: "Noise",
    name: "noise",
  },
  {
    alt: "Office",
    name: "office",
  },
  {
    alt: "Space",
    name: "space",
  },
];

function AccordionTrigger(props: { label: string }) {
  return (
    <Accordion.Trigger className={styles.accordionTrigger}>
      <span>
        {props.label}
        <Arrow />
      </span>
      <Separator orientation={"horizontal"} />
    </Accordion.Trigger>
  );
}

function BackgroundSelector() {
  return (
    <Accordion.Root
      type={"single"}
      defaultValue={"images"}
      className={styles.accordionRoot}
    >
      <Accordion.Item value={"images"} className={styles.accordionOpen}>
        <AccordionTrigger label={"Images"} />
        <Accordion.Content>
          <ImageSelector images={IMAGES} />
        </Accordion.Content>
      </Accordion.Item>
      <Accordion.Item value={"solidColours"} disabled>
        <AccordionTrigger label={"Solid Colours"} />
      </Accordion.Item>
      <Accordion.Item value={"videos"} disabled>
        <AccordionTrigger label={"Videos"} />
      </Accordion.Item>
    </Accordion.Root>
  );
}

BackgroundSelector.displayName = "BackgroundSelector";

export default BackgroundSelector;
