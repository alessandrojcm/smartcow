import Image from "next/image";
import classNames from "classnames";

import styles from "./MainVideoScreen.module.scss";
import { getImageSrc } from "../../utils";

interface MainVideoScreenProps {
  selectedImage: string;
  alt: string;
  className?: string;
}

const DEFAULT_DESCRIPTION =
  "Type or paste your videoscript here. " +
  "You can also request a translation of an English script to any of 27 other languages";

function MainVideoScreen(props: MainVideoScreenProps) {
  return (
    <section className={classNames(styles.container, props.className)}>
      <figure>
        <Image
          width={750}
          height={500}
          layout={"intrinsic"}
          alt={props.alt}
          src={getImageSrc(props.selectedImage)}
        />
        <figcaption>Preview</figcaption>
      </figure>
      <div className={styles.description}>
        <div>
          <textarea defaultValue={DEFAULT_DESCRIPTION} />
          <button>Listen</button>
        </div>
      </div>
    </section>
  );
}

MainVideoScreen.displayName = "MainVideoScreen";

export default MainVideoScreen;
