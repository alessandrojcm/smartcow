import * as Dialog from "@radix-ui/react-dialog";
import * as VisuallyHidden from "@radix-ui/react-visually-hidden";
import { useState } from "react";

import styles from "./VideoDescription.module.scss";
import ArrowUp from "@public/icons/arrow-up.svg";
import Input from "@common/components/Input";
import Tag from "@common/components/Tag";
import Separator from "@common/components/Separator";
import commonStyles from "@common/styles/common-clasess.module.scss";

interface EditOverlayProps {
  onClose: () => void;
  title: string;
  setTitle: (text: string) => void;
}

const TEXT_DEFAULT_VALUE =
  "Fusce quis magna vel ex pellentesque consequat sed et turpis. Vivamus bibendum rutrum euismod. Sed non sagittis est, semper";

function EditOverlay(props: EditOverlayProps) {
  const [description, setDescription] = useState(TEXT_DEFAULT_VALUE);
  return (
    <Dialog.Portal>
      <Dialog.Overlay className={styles.videoOverlay} />
      <Dialog.Content
        className={styles.videoOverlayContent}
        onPointerDownOutside={(e) => e.preventDefault()}
      >
        <VisuallyHidden.VisuallyHidden asChild>
          <Dialog.Description>Video edit overlay</Dialog.Description>
        </VisuallyHidden.VisuallyHidden>
        <section className={styles.videoOverlayControls}>
          <Input
            type={"text"}
            onChange={(e) => props.setTitle(e.target.value)}
            value={props.title}
            name={"Video Title"}
          />
          <textarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <div>
            <Tag aria-label={"toggle Email tag"}>Email</Tag>
            <Tag aria-label={"toggle Marketing tag"}>Marketing</Tag>
            <Tag aria-label={"toggle Greeting tag"}>Greeting</Tag>
            <Tag aria-label={"toggle Email tag"}>Email</Tag>
            <Tag aria-label={"toggle Marketing tag"}>Marketing</Tag>
            <Tag aria-label={"toggle Greeting tag"}>Greeting</Tag>
          </div>
          <Dialog.Close className={styles.saveButton}>Save</Dialog.Close>
        </section>
      </Dialog.Content>
    </Dialog.Portal>
  );
}

function VideoDescription() {
  const [videoName, setVideoName] = useState("Saying hi to my customers");
  const [isOverlayOpen, setIsOverlayOpen] = useState(false);
  return (
    <Dialog.Root onOpenChange={setIsOverlayOpen} open={isOverlayOpen}>
      <header className={styles.videoDescription}>
        {!isOverlayOpen ? (
          <>
            <span>
              <span style={{ marginRight: "auto" }}>
                <h1>{videoName}</h1>
                <Dialog.Trigger className={styles.arrowButton}>
                  <ArrowUp />
                </Dialog.Trigger>
              </span>
              <span>
                <button className={styles.cancelButton}>Cancel</button>
                <button className={commonStyles.formButton}>Save</button>
              </span>
            </span>
          </>
        ) : (
          //  avoiding cls
          <div style={{ height: "1.375rem" }} />
        )}
        <Separator />
      </header>
      <EditOverlay
        onClose={() => setIsOverlayOpen(false)}
        setTitle={setVideoName}
        title={videoName}
      />
    </Dialog.Root>
  );
}

VideoDescription.displayName = "VideoDescription";

export default VideoDescription;
