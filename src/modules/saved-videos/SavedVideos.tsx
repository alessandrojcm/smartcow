import Image from "next/image";
import Head from "next/head";

import Tag from "@common/components/Tag";
import styles from "./SavedVideo.module.scss";
import Separator from "@common/components/Separator";
import Options from "@public/icons/options.svg";
import commonStyles from "@common/styles/common-clasess.module.scss";

function SavedVideos() {
  return (
    <>
      <Head>
        <title>Saved Videos</title>
      </Head>
      <main className={styles.savedVideos}>
        <header className={commonStyles.headerWithButtons}>
          <span>
            <h1>Saved Videos</h1>
            <button className={styles.formButton}>Create New</button>
          </span>
          <Separator />
        </header>
        <section>
          <div className={styles.thumbnailContainer}>
            <figure>
              <Image
                src={"/images/YoYo.png"}
                layout={"intrinsic"}
                alt={"Saying Hi to users!"}
                width={350}
                height={250}
              />
              <figcaption>Saying Hi to users!</figcaption>
              <button>
                <Options />
              </button>
            </figure>
            <span>
              <Tag aria-label={"Email tag"} disabled>
                Email
              </Tag>
              <Tag aria-label={"Marketing tag"} disabled>
                Marketing
              </Tag>
              <Tag aria-label={"Greeting tag"} disabled>
                Greeting
              </Tag>
            </span>
          </div>
        </section>
      </main>
    </>
  );
}

SavedVideos.displayName = "SavedVideos";

export default SavedVideos;
