import styles from "./Billing.module.scss";
import Invoice from "@public/icons/invoice.svg";
import ArrowDownFa from "@public/icons/arrow-down-fa.svg";
import IconButton from "@common/components/IconButton/IconButton";

function Billing() {
  return (
    <div className={styles.billingContainer}>
      <table className={styles.billingTable}>
        <thead>
          <tr>
            <th>Reference id</th>
            <th style={{ display: "flex", alignItems: "center" }}>
              Date
              <IconButton icon={ArrowDownFa} />
            </th>
            <th>Amount</th>
            <th>Invoice</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>4571222f6rthswfg9981fr55</td>
            <td>7/12/2020</td>
            <td>&#36;28</td>
            <td>
              <IconButton icon={Invoice} />
            </td>
          </tr>
          <tr>
            <td>4571222f6rthswfg9981fr55</td>
            <td>7/12/2020</td>
            <td>&#36;36</td>
            <td>
              <IconButton icon={Invoice} />
            </td>
          </tr>
          <tr>
            <td>4571222f6rthswfg9981fr55</td>
            <td>7/12/2020</td>
            <td>&#36;14</td>
            <td>
              <IconButton icon={Invoice} />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

Billing.displayName = "Billing";

export default Billing;
