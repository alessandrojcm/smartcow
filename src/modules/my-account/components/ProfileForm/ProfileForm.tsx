import * as Avatar from "@radix-ui/react-avatar";
import classNames from "classnames";

import styles from "./ProfileForm.module.scss";
import commonStyles from "@common/styles/common-clasess.module.scss";
import Input from "@common/components/Input";
import { useUser } from "@common/hooks/useUser";
import EditPencil from "@public/icons/edit-pencil.svg";
import IconButton from "@common/components/IconButton/IconButton";

function ProfileForm() {
  const user = useUser();
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      <div className={styles.profileForm}>
        <Avatar.Root className={styles.avatar}>
          <Avatar.Image src={"/images/ProfileAvatar.png"} />
        </Avatar.Root>
        <IconButton className={styles.editIcon} icon={EditPencil} />
        <Input
          className={styles.firstnameInput}
          id={"firstname"}
          label={"First Name"}
          name={"firstname"}
          value={user?.firstname ?? ""}
        />
        <Input
          className={styles.lastnameInput}
          label={"Last Name"}
          name={"lastname"}
          value={user?.lastname ?? ""}
        />
        <Input
          className={styles.emailInput}
          label={"Email"}
          type={"email"}
          name={"email"}
          value={user?.email ?? ""}
        />
        <button className={classNames(commonStyles.formButton, styles.button)}>
          Save Changes
        </button>
      </div>
    </form>
  );
}

ProfileForm.displayName = "ProfileForm";

export default ProfileForm;
