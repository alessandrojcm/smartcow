import styles from "./Plans.module.scss";

function Plans() {
  return (
    <div className={styles.planContainer}>
      <div className={styles.planTier}>
        <h2>Free</h2>
        <ul>
          <li>&#10003; Pellentesque interdum libero et</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#x78; Pellentesque posuere</li>
        </ul>
        <span>
          <strong>&#36;</strong>
          <h3>0</h3>
        </span>
        <button>Downgrade</button>
      </div>
      <div className={styles.planTier}>
        <h2>Pro</h2>
        <ul>
          <li>&#10003; Pellentesque interdum libero et</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
        </ul>
        <span>
          <strong>&#36;</strong>
          <h3>12</h3>
        </span>
        <button>Downgrade</button>
      </div>
      <div className={styles.planTierSelected}>
        <h2>Team</h2>
        <ul>
          <li>&#10003; Pellentesque interdum libero et</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#x78; Pellentesque posuere</li>
        </ul>
        <span>
          <strong>&#36;</strong>
          <h3>23</h3>
        </span>
        <button>Current Plan</button>
      </div>
      <div className={styles.planTier}>
        <h2>Agency</h2>
        <ul>
          <li>&#10003; Pellentesque interdum libero et</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
          <li>&#10003; Pellentesque posuere</li>
        </ul>
        <span>
          <strong>&#36;</strong>
          <h3>43</h3>
        </span>
        <button>Upgrade</button>
      </div>
    </div>
  );
}

Plans.displayName = "Plan";

export default Plans;
