import Head from "next/head";

import styles from "./MyAccount.module.scss";
import ProfileForm from "./components/ProfileForm";
import Plans from "./components/Plans";
import Billing from "./components/Billing";
import {
  TabContent,
  TabList,
  TabsRoot,
  TabTrigger,
} from "@common/components/Tabs";
import Separator from "@common/components/Separator";

function MyAccount() {
  return (
    <>
      <Head>
        <title>My Account</title>
      </Head>
      <main className={styles.profileContainer}>
        <header>
          <h1>My Account</h1>
          <Separator />
        </header>
        <TabsRoot defaultValue="profile" orientation="vertical">
          <TabList aria-label="profile section">
            <TabTrigger value="profile">Profile</TabTrigger>
            <TabTrigger value="my-plan">My Plan</TabTrigger>
            <TabTrigger value="billing">Billing</TabTrigger>
          </TabList>
          <TabContent value="profile" className={styles.tabContent}>
            <ProfileForm />
          </TabContent>
          <TabContent value="my-plan" className={styles.tabContent}>
            <Plans />
          </TabContent>
          <TabContent value="billing">
            <Billing />
          </TabContent>
        </TabsRoot>
      </main>
    </>
  );
}

MyAccount.displayName = "MyAccount";

export default MyAccount;
