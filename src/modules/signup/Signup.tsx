import Head from "next/head";
import Link from "next/link";

import styles from "./Signup.module.scss";
import Input from "@common/components/Input";

function Signup() {
  return (
    <>
      <Head>
        <title>Sign up</title>
      </Head>
      <main className={styles.signup}>
        <h1>Sign up</h1>
        <form onSubmit={(e) => e.preventDefault()}>
          <div className={styles.signupForm}>
            <Input
              label={"Full name"}
              name={"name"}
              required
              placeholder={"Enter your name"}
            />
            <Input
              label={"Email address"}
              name={"email"}
              required
              placeholder={"Enter your email"}
            />
            <Input
              label={
                <span className={styles.passwordLabel}>
                  <p>Password</p>
                  <p>Strong</p>
                </span>
              }
              name={"password"}
              placeholder={"Enter your password"}
              required
              type={"password"}
            />
            <button>Sign up</button>
            <p>
              Already an user? <Link href={"/signin"}>Login</Link>
            </p>
          </div>
        </form>
      </main>
    </>
  );
}

Signup.displayName = "Signup";

export default Signup;
