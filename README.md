# Smart Cow front end assignment

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgithub.com%2Falessandrojcm%2Fsmartcow)

## Description

This project uses Next.js and React 18 and is deployed to Vercel.

## Libraries used

* [Radix UI](https://www.radix-ui.com/) to build components that are fully accessible and battle-tested across browsers.
* [Classnames](https://www.npmjs.com/package/classnames), for conditionally applying styles.
* [Cookies](https://www.npmjs.com/package/cookies) for setting the session cookies.
* [ky](https://www.npmjs.com/package/ky) a tiny (3kb) wrapper over fetch.
* [normalize.css](https://www.npmjs.com/package/normalize.css) as css reset.

## File structure

The file structure is inspired in [Next Right Now](https://unlyed.github.io/next-right-now/reference/folder-structure)

## Setting up the project

To run the project:

* Install dependencies with `yarn`
* Configure the credentials with the env variables file, create an `.env` file, copy the contents of the `.env.example` file and set the email and password you want to use as credentials.
* Run with `yarn dev`

Live example in: https://smartcow.alessandrojcm.dev (email: 13bala90@gmail.com and password: password)