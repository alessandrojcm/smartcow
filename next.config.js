const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

const nextConfig = {
  images: {
    deviceSizes: [640, 768, 1024, 1280, 1440, 1536],
  },
  webpack(config) {
    config.module.rules.push(
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        use: ["@svgr/webpack"],
      },
      {
        test: /src\/.+index\.tsx?$/i,
        sideEffects: false,
      }
    );

    return config;
  },
};

module.exports = withBundleAnalyzer(nextConfig);
